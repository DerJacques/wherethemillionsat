var http = require('http');
var fs = require('fs');

function getExchangeRates(callback) {

    return http.get({
        host: 'www.apilayer.net',
        path: '/api/live?access_key=501a1ed8ccaa7f96614805c2db81318c'
    }, function(response) {
        // Continuously update stream with data
        var body = '';
        response.on('data', function(d) {
            body += d;
        });
        response.on('end', function() {

            // Data reception is done, do whatever with it!
            var parsed = JSON.parse(body);
            callback(parsed)
        });
    });

}

getExchangeRates(response => {
    fs.writeFile("./src/rates.json", JSON.stringify(response.quotes), function(err) {
    if(err) {
        return console.log(err);
    }

    console.log("Exchange rates have been updated!");
});
})
